<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require dirname(__FILE__).'/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Extemplate
{

	public $return_data = '';

	public function __construct()
	{

		$this->base_url = ee()->config->item('extemplate_base_url') ? ee()->config->item('extemplate_base_url') : '';

	}

	/**
	 * Module tag
	 * @return html 	The html with your templates parsed
	 */
	public function get()
	{
		// Get params
		$url = ee()->TMPL->fetch_param('url', NULL);
		
		$this->use_base = $this->setEEParam( ee()->TMPL->fetch_param('env_base') );

		if (is_null($url) || empty($url)) {

			show_error('EXTEMPLATE: Missing params: url');
			exit();

		}

		// Get ssl
		$this->use_ssl = $this->setEEParam( ee()->TMPL->fetch_param('ssl') );

		// Setting the use of template tags
		$this->is_template = ee()->TMPL->fetch_param('is_template', FALSE);

		// Get base if set
		if (ee()->TMPL->fetch_param('base'))
		{

			$this->base_url = $this->getBaseFromSession(ee()->TMPL->fetch_param('base'));
			$this->use_base = TRUE;

		}

		// Get tag data
		$tagData = ee()->TMPL->tagdata;
		
		// Get the template
		$template = $this->getTemplate($url);

		// Parse the template
		$parsed = $this->parseTemplate($tagData, $template);

		// Return the completed template
		return $parsed;
	}

	/**
	 * sets base, for calling with specific templates
	 * @param name        string       name of base
	 * @param url         string       url of base
	 */
	public function set()
	{
		// Set variables
		$name = ee()->TMPL->fetch_param('name', NULL);
		$url = ee()->TMPL->fetch_param('url', NULL);
		
		// if they're set
		if( !is_null( $name ) && !is_null( $url ) )
		{

			// Set the session
			$data[] = [$name, $url];

			if (isset(ee()->session->cache['extemplate'])) {

				ee()->session->cache['extemplate'] = array_merge(ee()->session->cache['extemplate'], $data);

			} else {

				ee()->session->cache['extemplate'] = $data;

			}

		} else {

			// Throw error
			show_error('EXTEMPLATE: Base set missing variable. Set both name and url.');

		}

	}

	// Private Functions
	/**
	 * gets base url from session with name
	 * @param  string $name name of base url stored in session data
	 * @return string       url base
	 */
	private function getBaseFromSession($name)
	{
		$session = ee()->session->cache['extemplate'];

		foreach ($session as $session_data) {
			if ($session_data[0] == $name)
			{

				return $session_data[1];

			}

		}

		return FALSE;

	}
	
	/**
	 * gets Template
	 * @param  string $url url of template
	 * @return html
	 */
	private function getTemplate($url)
	{
		if ($this->isItCached($url)) {

			return $this->getTemplateFromCache($url);

		} else {

			return $this->getTemplateFromURL($url);

		}

	}

	/**
	 * gets template from ee cache
	 * @param  string $url url of template
	 * @return html
	 */
	private function getTemplateFromCache($url)
	{

		$name = $this->debase($url);

		return ee()->cache->get('/extemplate/'.$name);

	}

	/**
	 * gets content of the URL
	 * @param  url $url 		url of template
	 * @param  string $name 	name of variable. Used for caching
	 * @return html
	 */
	private function getTemplateFromURL($url)
	{

		// Guzzle URL with optional base
		try {
			
			$client = $this->use_base ? new Client(['base_uri' => $this->base_url]) : new Client();

		} catch (Exception $e) {

			show_error('EXTEMPLATE: Guzzle base issue: ' . $this->base_url);

		}

		try {
			
			$res = $client->request('GET', $url, ['verify' => $this->use_ssl]);
		
		} catch (Exception $e) {

			show_error('EXTEMPLATE: Guzzle 404: ' . ($this->base_url ? $this->base_url . $url : $url));

		}

		if ($res->getStatusCode() < 400) {

			// Cache for later use
			$data = $res->getBody();

			$name = $this->debase($url);

			ee()->cache->save('/extemplate/'.$name, $data->getContents());

			// Return it
			return $res->getBody();

		} else {

			// Return FALSE, we'll need to die
			return FALSE;
		}

	}

	/**
	 * checks if template is cached
	 * @param  string  $url
	 * @return boolean 
	 */
	private function isItCached($url)
	{
		
		$name = $this->debase($url);

		return $result = ee()->cache->get('/extemplate/'.$name) ? TRUE : FALSE;

	}

	/**
	 * Parse tag data and adds it to the template
	 * @param  string $tagData  EE Tag data in the template
	 * @param  string $template template pulled from URL
	 * @return $parsed           parsed template data
	 */
	private function parseTemplate($tagData, $template)
	{
		// Get tags from tag data
		preg_match_all('/\{(\/)?extemplate:(.*?)\}/', $tagData, $matches);
		$tags = $matches[0];
		$tagNames = $matches[1];
		
		// Get open and close tags
		$opentags = $closetags = [];
		foreach ($tags as $key => $value) {

			if($key&1) {

				$closetags[] = $value;

			} else {
				
				$opentags[] = $value;

			}

		}

		// Get everything between tags
		for ($i=0; $i < count($opentags); $i++) { 

			$tagContent[] = $this->getBetween($tagData, $opentags[$i], $closetags[$i]);

			// Get double bracket template tags
			$templatetags[] = '[['.$this->getBetween($opentags[$i], '{extemplate:','}').']]';

		}

		// Parse the template
		$parsed = $template;
		for ($i = 0; $i < count($opentags); $i++) { 

			$parsed = str_replace($templatetags[$i], $tagContent[$i], $parsed);

		}

		// Check the template
		$parsed = $this->checkTemplate($parsed);

		// Removed template tags
		$parsed = ($this->is_template ? $parsed : $this->removeTemplateTag($parsed));


		return $parsed;

	}

	/**
	 * Gets content between extemplate tags
	 * @param string $content full string
	 * @param string $start   start tag
	 * @param string $end     end tag
	 */
	private function getBetween($content, $start, $end)
	{
		
		$r = explode($start, $content);
		
		if (isset($r[1])){
		
			$r = explode($end, $r[1]);
		
			return $r[0];
		
		}
		
		return '';
	
	}

	/**
	 * checks template for missing tags
	 * @param  string $template template with parsed tags
	 * @return $template           Returns template. If missing a template tag, then it appends a notification to JS console.
	 */
	private function checkTemplate($template)
	{

		// Check if you are missing any templates
		preg_match_all('/\[\[(.*?)\]\]/', $template, $matches);

		if (!empty($matches[0])) {

			$allTags = implode(', ', $matches[0]);

			show_error('EXTEMPLATE: Missing tags = '.$allTags);

			exit();

		}

		return $template;

	}

	/**
	 * makes url name pretty
	 * @param  string $url url of template
	 * @return string      name of url without slashes
	 */
	private function debase($url)
	{

		return str_replace('/', '-', $url);

	}

	/**
	 * Removed Template tags
	 * @param  string $data 	html with the template tags within
	 * @return string       	html with the template tags removed
	 */
	private function removeTemplateTag($data)
	{

		$content = str_replace(array("<template>","</template>"), "", $data);

		return $content;
		
	}

	/**
	 * repetitive function to set true or false for ee param
	 * @param string $data
	 * @return  boolean
	 */
	private function setEEParam($data)
	{

		if ( !empty($data) && !is_null($data) ) {

			switch (strtolower($data)) {

				case 'false':
					return FALSE;
					break;
				
				default:
					return TRUE;
					break;

			}

		} else {

			// Defaults to true
			return TRUE;

		}

	}

}