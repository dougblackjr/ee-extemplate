<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Extemplate_upd {

	var $version = '1.0.0';

	function install()
	{

		// Database setup
		ee()->load->dbforge();

		// Create module table
		ee()->dbforge->drop_table('extemplate');

		$fields = array(
			'name' => array(
				'type' => 'TEXT',
			),
			'url' => array(
				'type' => 'TEXT',
			)
		);

		ee()->dbforge->add_field('id');
		ee()->dbforge->add_field($fields);
		ee()->dbforge->create_table('extemplate');

		unset($fields);

		// APP INFO
		$data = array(
		 'module_name' => 'Extemplate' ,
		 'module_version' => $this->version,
		 'has_cp_backend' => 'y'
		);

		ee()->db->insert('modules', $data);
		ee()->load->library('layout');

		return true;

	}

	function update($current = '')
	{
		if (version_compare($current, '2.0', '='))
		{
				return FALSE;
		}

		if (version_compare($current, '2.0', '<'))
		{
				// Do your update code here
		}

		return TRUE;
	}

	function uninstall() {
		ee()->load->dbforge();

		ee()->dbforge->drop_table('extemplate');

		ee()->db->where('module_name', 'Extemplate');
		ee()->db->delete('modules');

		ee()->load->library('layout');

		return true;
	}
}

/* End of file upd.extemplate.php */