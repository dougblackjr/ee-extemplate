<?php

/**
 * ExTemplate: Plugin to add external html files to EE
 *
 * @package        extemplate
 * @author         Doug Black <dougblackjr@gmail.com>
 * @link           http://github.com/dougblackjr
 */

return array(
	'author'         => 'Doug Black',
	'author_url'     => 'https://github.com/dougblackjr',
	'docs_url'       => 'https://bitbucket.org/triplenerdscore/extemplate/overview',
	'name'           => 'ExTemplate',
	'description'    => 'Add external html files to EE',
	'version'        => '1.0.0',
	'namespace'      => 'Abs\Extemplate',
	'settings_exist' => FALSE
);

/* End of file addon.setup.php */