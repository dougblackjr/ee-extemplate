# ExTemplate

## Get external HTML resources with tags to parse, parse the tags, and put it on the page

### EXAMPLE:
		{exp:extemplate:get url="http://www.example.com" env_base="FALSE"}
			{extemplate:tag}data{/extemplate:tag}
			{extemplate:tag2}{exp:ee_code}{/extemplate:tag2}
			{extemplate:tag3}text and such{/extemplate:tag3}
		{/exp:extemplate:get}

### TEMPLATE:
		<div>
			<h1>[[tag]]</h1>
			<h3>[[tag2]]</h3>
			<p>[[tag3]]</p>
		</div>

### EE TAG PARAMS:
`url`: Url for the site you will be calling (REQUIRED)

`env_base`: Boolean whether to use base url set in config (DEFAULT: TRUE). You can set a base url in your config file:
`$env_config['extemplate_base_url'] = 'http://example.com/';`
*YOU MUST* ensure the URL ends with a slash.

### EXTERNAL TEMPLATES
External templates should include your tag names surround by two brackets on each side.

ie.
`[[tag]]`

### FUNCTIONS:
Get remote HTML templates
Parse tags in templates
Cache urls and templates somewhere
Alert if you don't set a variable in the template


## EXAMPLE TEMPLATE
[Outside template at PasteBin](https://pastebin.com/raw/vz9wFE6w)

		<div class="card" style="width: 300px;">
			<div class="card-divider">
				[[subtitle]]
			</div>
			<img src="[[image]]">
			<div class="card-section">
				<h4>[[title]]</h4>
				<p>[[snippet]]</p>
			</div>
		</div>

Interal EE template

	{exp:extemplate:get url="https://pastebin.com/raw/vz9wFE6w" use_base="FALSE"}
		{extemplate:title}{title}{/extemplate:title}
		{extemplate:subtitle}{subtitle}{/extemplate:subtitle}
		{extemplate:image}{image}{/extemplate:image}
		{extemplate:snippet}{exp:chopper words="50"}{body}{/exp:chopper}{/extemplate:snippet}
	{/exp:extemplate:get}


## FOR TESTING
`ssl="FALSE"`
This is used to bypass ssl certificates. Use this for local testing, or known ssl certificate errors. USE AT YOUR OWN RISK, especially in production. Defaults to TRUE.

`is_template="FALSE"`
This is to use HTML5 template tags for use with calling in Javascript. Defaults to FALSE.